import test from 'ava';
import codata from "./../../src/index.js";
import fs from "fs";


codata.pathToModules = './tests/v1.3.0/codata';

let getSample = name => {
	return JSON.parse(fs.readFileSync(`${process.cwd()}/${codata.pathToModules}/samples/${name}.result.json`));
}


//---------------------------------------------------------------------
// moduleToFileNames
//---------------------------------------------------------------------

let samples = [
	['project_development',
		[	`${codata.pathToModules}/project_development.yaml`,
			`${codata.pathToModules}/project_development.json`,
			`${codata.pathToModules}/project.yaml`,
			`${codata.pathToModules}/project.json`
		]
	]
];


samples.forEach((sample) => {
	test.serial (`moduleToFileNames '${sample[0]}'`, t => {
		t.deepEqual (
			codata.moduleToFileNames (sample[0]),
			sample[1]
		);
	});
});


//---------------------------------------------------------------------
// Load module test
//---------------------------------------------------------------------

test.serial (`project_development`, async t => {

	codata.templatingData = {PROJECT: 'My_Project'};

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});


test.serial (`project_beta`, async t => {

	codata.templatingData = {PROJECT: 'My_Project'};

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});


//---------------------------------------------------------------------
// replace module test
//---------------------------------------------------------------------

test.serial (`project_replaced`, async t => {
	let data	= await codata.load (t.title);
	t.deepEqual (data, {wholeNewObject: true});
});

test.serial (`project_replaced_onready`, async t => {
	let data	= await codata.load (t.title);
	t.deepEqual (data, {wholeNewObjectOnReady: true});
});

