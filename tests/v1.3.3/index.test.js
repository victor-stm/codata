import test from 'ava';
import codata from "./../../src/index.js";
import fs from "fs";


codata.pathToModules = './tests/v1.3.3/codata';

let getSample = name => {
	return JSON.parse(fs.readFileSync(`${process.cwd()}/${codata.pathToModules}/samples/${name}.result.json`));
}


//---------------------------------------------------------------------
// PROPS
//---------------------------------------------------------------------

test.serial (`props_dev`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});


test.serial (`props_beta`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});


//---------------------------------------------------------------------
// ENVIRONMENT
//---------------------------------------------------------------------

test.serial.skip (`env_simple`, async t => {						// Changed in 1.5.0

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

test.serial (`env_undefined`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});


//---------------------------------------------------------------------
// ORDER
//---------------------------------------------------------------------

test.serial (`order`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});


//---------------------------------------------------------------------
// LOCAL
//---------------------------------------------------------------------

test.serial (`loc_normal`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});


test.serial (`loc_undefined`, async t => {

	try {
		let data = await codata.load (t.title);
		t.fail ('Exception expected.');
	} catch (e) {
		if (!t.is (e.message, "Templating error: Local variable '${local.topA}' not found")) console.log (e.message);
	}

});

