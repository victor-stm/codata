import test from 'ava';
import codata from "./../../src/index.js";
import fs from "fs";


codata.pathToModules = './tests/v1.5.0/codata';

let getSample = name => {
	return JSON.parse(fs.readFileSync(`${process.cwd()}/${codata.pathToModules}/samples/${name}.result.json`));
}


//---------------------------------------------------------------------
// ENVIRONMENT
//---------------------------------------------------------------------

test.serial (`env_file`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

test.serial (`env_module`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

test.serial (`env_moduleOnly`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

