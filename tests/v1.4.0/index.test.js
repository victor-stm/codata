import test from 'ava';
import codata from "./../../src/index.js";
import fs from "fs";


codata.pathToModules = './tests/v1.4.0/codata';

let getSample = name => {
	return JSON.parse(fs.readFileSync(`${process.cwd()}/${codata.pathToModules}/samples/${name}.result.json`));
}


//---------------------------------------------------------------------
// ENVIRONMENT
//---------------------------------------------------------------------

test.serial.skip (`env_file`, async t => {						// Changed in 1.5.0

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

test.serial.skip (`env_module`, async t => {					// Changed in 1.5.0

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

test.serial.skip (`env_moduleOnly`, async t => {				// Changed in 1.5.0

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

