import test from 'ava';
import codata from "./../../src/index.js";
import fs from "fs";


codata.pathToModules = './tests/v1.6.0/codata';

let getSample = name => {
	return JSON.parse(fs.readFileSync(`${process.cwd()}/${codata.pathToModules}/samples/${name}.result.json`));
}


//---------------------------------------------------------------------
// ENVIRONMENT
//---------------------------------------------------------------------

test.serial (`env_default`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

test.serial (`env_no-arr`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

test.serial (`env_no-obj`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

test.serial (`env_no-top`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

test.serial (`env_no-name`, async t => {

	let sample	= getSample (t.title);
	let data	= await codata.load (t.title);

	if (!t.deepEqual (data, sample)) console.log (JSON.stringify(data));
});

