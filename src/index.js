import jsYaml from "js-yaml";
import fileCascade from "file-cascade";
import path from "path";


export class Codata
{
	/** @type {String} **/			pathToModules;
	/** @type {String} **/			modulesSplitBy;
	/** @type {String[]} **/		dataExtensions;
	/** @type {Object} **/			templatingData;
	/** @type {Object} **/			environmentInfo;

	/** @type {String[]} **/		cwd;


	constructor ()
	{
		this.cwd				= [];
		this.modulesSplitBy		= '_';
		this.pathToModules		= './codata';
		this.dataExtensions		= ['yaml', 'json'];
		this.templatingData		= {};
		this.environmentInfo	= {};
	}


	/**
	 * @param {String} module
	 * @return {Promise<Object>}
	 */
	async load (module)
	{
		this.cwd				= [this.pathToModules];
		this.environmentInfo	= {};

		let obj		= await this.loadProcess (module);

		if (obj.__onready) {
			let filename = obj.__onready;
			delete obj.__onready;
			let res = await this.execScript (obj, filename, module);
			if (res !== undefined) obj = res;
		}

		this.cwd = [];

		return obj;
	}


	/**
	 * @param {String} module
	 * @param {Object} environment
	 * @return {Promise<Object>}
	 */
	async loadProcess (module, environment={})
	{
		let superObj		= {};
		let obj				= {};
		let fileObj;
		let extension;

		// console.log (`**** loadProcess: ${module}`);

		//---------------------------------------------------------------------
		// Set working directory in stack at first position, so cwd[0] is last
		//---------------------------------------------------------------------

		let folder	= path.dirname (module);
		module		= path.basename (module);
		folder = (folder === '.') ? '' : '/' + folder;
		this.cwd.unshift (`${this.cwd[0]}${folder}`);


		//---------------------------------------------------------------------
		// split module to filename and specified field name
		//---------------------------------------------------------------------

		let parts		= module.split ('@');
		let fieldName	= undefined;
		if (parts.length === 2) {
			module		= parts[0];
			fieldName	= parts[1];
		}


		//---------------------------------------------------------------------
		// Load config file
		//---------------------------------------------------------------------

		let files	= this.moduleToFileNames (module);
		let content	= fileCascade.readFileSync (files);
		let file	= fileCascade.lastLoadedFile;

		//---------------------------------------------------------------------
		// parse format && get module
		//---------------------------------------------------------------------

		extension	= path.extname (file);

		delete this.templatingData.local;
		delete this.templatingData.env;

		try {
			fileObj = this.parseFormat (content, extension);

			// local
			if (fileObj.local) this.templatingData.local = fileObj.local;

			// environment
			environment = this.deepMerge (process.env, environment);			// environment from derived module
			this.templatingData.env = environment;


			//---------------------------------------------------------------------
			// File level environment variables
			//---------------------------------------------------------------------

			if (fileObj.__environment) {

				let envUpdated = false;

				for (let v in fileObj.__environment) {

					// check if variable already defined at module level and keep info of this definition
					let envInfo = this.environmentInfo[v] ? this.environmentInfo[v] : this.environmentInfo[v] = [new EnvironmentInfo ('start', module)];
					if (envInfo[0].level !== 'start') continue;
					envInfo.unshift (new EnvironmentInfo ('file', file));

					// process variable
					fileObj.__environment[v]	= this.templating (fileObj.__environment[v]);
					envInfo[0].value			= fileObj.__environment[v];
					process.env[v]				= fileObj.__environment[v];
					//console.log (`update ${v}=${fileObj.__environment[v]}`);
					//console.log (envInfo);

					envUpdated = true;
				}

				if (envUpdated) {
					environment = this.deepMerge (environment, obj.__environment);
					return this.loadProcess (module, environment);
				}
			}
		} catch (e) {
			console.log ('Unable to parse file before templating, local vars unavailable', e);
		}
		content		= this.templating (content);
		fileObj		= this.parseFormat (content, extension);

		if (!fileObj[module]) throw new Error (`Section ${module} not found`);
		obj = fileObj[module];


		//---------------------------------------------------------------------
		// Use object's environment variables
		//---------------------------------------------------------------------


		if (obj.__environment) {

			let envUpdated = false;

			for (let v in obj.__environment) {

				// check if variable already defined at module level and keep info of this definition
				let envInfo = this.environmentInfo[v] ? this.environmentInfo[v] : this.environmentInfo[v] = [new EnvironmentInfo ('start', module)];
				if (envInfo[0].level === 'module') continue;										// environment variable can be defined only in first module
				if (envInfo[0].level === 'file' && envInfo[0].info !== file) continue;				// module can redefine only variables in current file
				envInfo.unshift (new EnvironmentInfo ('module', module));

				// process variable
				obj.__environment[v]	= this.templating (obj.__environment[v]);
				envInfo[0].value		= obj.__environment[v];
				process.env[v]			= obj.__environment[v];
				//console.log (`update ${v}=${obj.__environment[v]}`);
				//console.log (envInfo);

				envUpdated = true;
			}

			if (envUpdated) {
				environment = this.deepMerge (environment, obj.__environment);
				return this.loadProcess (module, environment);
			}

			delete obj.__environment;
		}


		//---------------------------------------------------------------------
		// Import and merge super object (extends)
		//---------------------------------------------------------------------

		if (obj.__extends) {

			let traits = obj.__extends;
			delete obj.__extends;

			if (traits instanceof String || typeof (traits) === 'string') traits = [traits];
			if (typeof traits === 'object' && !Array.isArray(traits) && traits !== null) traits = [traits];
			if (!(traits instanceof Array)) throw new Error (`${module}.__extends should be a String[] or String`);

			let tempObj = {}; // build all super modules in this temp object to keep their execution order

			for (let i = 0; i < traits.length; ++i) {
				let trait = traits[i];
				let props;
				if (trait instanceof String || typeof (trait) === 'string'){
					props = {};
				} else {
					props = trait.props ? trait.props : {};
					trait = trait.module;
				}
				this.templatingData.props	= props;
				superObj	= await this.loadProcess (trait, environment);
				tempObj		= this.deepMerge (tempObj, superObj);  // super modules with greater index should overwrite previously added, so arguments order is inverted
			}

			obj = this.deepMerge (tempObj, obj);
		}


		//---------------------------------------------------------------------
		// exec __constructor
		//---------------------------------------------------------------------

		/** @deprecated v2.0.0 **/
		if (obj.__script) {
			if (!obj.__constructor) obj.__constructor = obj.__script;
			delete obj.__script;
		}

		if (obj.__constructor) {
			let filename = obj.__constructor;
			delete obj.__constructor;
			let res = await this.execScript (obj, filename, module);
			if (res !== undefined) obj = res;
		}


		//---------------------------------------------------------------------
		// Place result in target field (if specified)
		//---------------------------------------------------------------------

		if (fieldName) {
			let parts = fieldName.split ('.');
			let target = {};
			let res = target;
			parts.forEach((item, i, arr) => {
				target = target[item] = (i === arr.length-1) ? obj : {};
			})
			obj = res;
		}


		//---------------------------------------------------------------------
		// update cwd & return result
		//---------------------------------------------------------------------

		this.cwd.shift ();

		return obj;
	}


	/**
	 * @param {Object} obj
	 * @param {String} filename
	 * @param {String} module
	 */
	async execScript (obj, filename, module)
	{
		let file = filename;
		let parts = filename.split ('#');
		if (parts.length === 2) {
			file	= parts[0];
			module	= parts[1];
		}

		/** @type {Function} **/
		let script = (await import(`file://${process.cwd()}/${this.cwd[0]}/${file}`))[module];
		if (!script) throw new Error(`${module}.${filename} JS function is missing`);
		return script.apply (obj);
	}


	/**
	 * @param {String} module
	 */
	moduleToFileNames (module)
	{
		let parts = module.split (this.modulesSplitBy);
		let files = [];
		let cwd = this.cwd[0] ? this.cwd[0] : this.pathToModules;

		if (parts[0] === '') {
			parts[1] = this.modulesSplitBy + parts[1];
			parts.shift ();
		}

		for (let i = parts.length - 1; i >= 0; --i) {
			for (let k = 0; k < this.dataExtensions.length; ++k) {
				files.push (`${cwd}/${parts.slice (0, i + 1).join (this.modulesSplitBy)}.${this.dataExtensions[k]}`);
			}
		}

		return files;
	}


	/** Replace ${VAR} in loaded file to its value
	 * @param {String} content
	 * @param {Object} data
	 * @param {String} prefix
	 * @return {String}
	 */
	templating (content, data=this.templatingData, prefix='')
	{
		if (!content.replaceAll) return content;

		for (const v in data) {
			if (typeof data[v] === 'object' && data[v] !== null) {
				content = this.templating (content, data[v], prefix + v + '.');
				continue;
			}
			content = content.replaceAll ('${' + prefix + v + '}', data[v]);
		}

		if (prefix === '') {
			content = content.replaceAll (/\${props\.([^\}]*)}/g, "$1");

			let error = '';

			let local = content.match (/\${local\.([^}]*)}/);
			if (local !== null) {
				let message = `Local variable '${local[0]}' not found`;
				//console.log (message);
				//console.log (this.templatingData.local);
				error = message;
			}

			let env = content.match (/\${env\.([^}]*)}/);
			if (env !== null) {
				let message = `Environment variable '${env[0]}' not found`;
				//console.log (message);
				//console.log (this.templatingData.env);
				//error = message;
			}

			if (error) {
				throw new Error (`Templating error: ${error}`);
			}
		}
		return content;
	}


	/**
	 * @param {String} content
	 * @param {String} extension
	 * @return {Object}
	 */
	parseFormat (content, extension)
	{
		switch (extension) {

			case '.yaml':
				return jsYaml.load (content);

			case '.json':
				return JSON.parse (content);

			default:
				throw new Error (`Unsupported file type: ${extension}`);
		}
	}


	/**
	 * @param {Object} superObj
	 * @param {Object} obj
	 */
	deepMerge (superObj, obj)
	{
		for (let v in obj) {
			switch (typeof superObj[v]) {
				case "object":
					if (superObj[v] === null) {
						superObj[v] = obj[v];
					} else {
						if (typeof obj[v] !== 'object' || obj[v] === null){			// v 1.6.0 - replace object and array with basic data types
							superObj[v] = obj[v];
						} else {
							superObj[v] = this.deepMerge (superObj[v], obj[v]);
						}
					}
					break;
				default:
					superObj[v] = obj[v];
			}
		}

		return superObj;
	}

}


class EnvironmentInfo {
	/** @type {String} **/		level;
	/** @type {String} **/		info;
	value;


	/**
	 * @param {String} level
	 * @param {String} info
	 */
	constructor (level, info)
	{
		this.level	= level;
		this.info	= info;
	}

}


let codata = new Codata ();
export default codata;